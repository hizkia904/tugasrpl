import java.util.Scanner;

public class DivisorApp{
    public void kerjakan(double angka1,double angka2){
        try{
            double awal = System.currentTimeMillis();
            double hasil = angka1/angka2;
            double akhir = System.currentTimeMillis();
            double waktu = akhir-awal;
            System.out.printf("Hasil : %.2f",hasil);
            System.out.println(" ,Waktu : "+waktu+ " millisecond");
        }catch(ArithmeticException e){
            System.out.println("Angka kedua tidak boleh 0");
        }
        
        
    }
}
class Main{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double angka1 = sc.nextDouble();
        double angka2 = sc.nextDouble();
        new DivisorApp().kerjakan(angka1, angka2);
    }
}